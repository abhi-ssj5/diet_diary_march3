//
//  RandomFunctions.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 02/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class RandomFunctions {
    
    static let shared = RandomFunctions()
    
    func color() -> UIColor {
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
    func number(value: Int) -> Int {
        return Int(arc4random_uniform(UInt32(value)))
    }
    
}
