//
//  Time.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class Time {
    
    static let shared = Time()
    
    func getTime() -> String {
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "hh:mm"
        let nsdate = NSDate()
        let time = dateFormat.string(from: nsdate as Date)
        
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        
        if hour > 11 {
            return time + " pm"
        } else {
            return time + " am"
        }
    }
    
}
