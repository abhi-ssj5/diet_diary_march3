//
//  TodayCellLabel.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

enum Section {
    case yourFood
    case foodConsumed
}

class TodayCellLabel {
    
    static let shared = TodayCellLabel()
    
    func attStringLabel(_ cellData: [String: String]?, section: Section) -> NSMutableAttributedString {
        
        let combination = NSMutableAttributedString()
        
        if let data = cellData {
            let str_1 = AttributedString.shared.attributeString(value: (data["name"]!), font: "Lato-Medium", size: 14, color: UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1))
            combination.append(str_1)
            
            switch section {
            case .yourFood:
                print("no time")
                
            case .foodConsumed:
                var pre:String = " | "
                if (data["btn"]?.isEqual("0"))! {
                    pre = " "
                }
                
                let str_2 = AttributedString.shared.attributeString(value: pre+(data["time"])!, font: "Lato-Regular", size: 14, color: UIColor(red: 0.76, green: 0.76, blue: 0.76, alpha: 1))
                combination.append(str_2)
            }
            
            let str = "\n"+data["quantity"]! + " " + data["quantityType"]! + " . " + data["value"]! + " Kcal"
            
            let str_3 = AttributedString.shared.attributeString(value: str , font: "Lato-Medium", size: 12, color: UIColor(red: 0.61, green: 0.61, blue: 0.61, alpha: 1))
            combination.append(str_3)
            
            return combination
        } else {
            return AttributedString.shared.attributeString(value: "Empty String", font: "Lato-Medium", size: 14, color: UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1))
        }
        
    }
    
}
