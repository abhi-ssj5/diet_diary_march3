//
//  CheckNil.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation

class CheckNil {
    
    static let shared = CheckNil()
    
    func string(string: String?) -> String {
        guard let value = string else {
            return ""
        }
        return value
    }
    
    func number(number: Int?) -> Int {
        guard let value = number else {
            return 0
        }
        return value
    }
    
}
