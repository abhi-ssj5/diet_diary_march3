//
//  AttributedString.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 02/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class AttributedString {
    
    static let shared = AttributedString()
    
    func attributeString(value: String?, font: String, size: Int) -> NSMutableAttributedString {
        
        if let str = value {
            return NSMutableAttributedString(string: str, attributes: [NSFontAttributeName: UIFont(name: font,size: CGFloat(size)) as Any])
        } else {
            return NSMutableAttributedString(string: " ", attributes: [NSFontAttributeName: UIFont(name: font,size: CGFloat(size)) as Any])
        }
    }
    
    func attributeString(value: String?, font: String, size: Int, color: UIColor) -> NSMutableAttributedString {
        var string:NSMutableAttributedString? = NSMutableAttributedString(string: " ", attributes: [NSFontAttributeName: UIFont(name: font,size: CGFloat(size)) as Any])
        
        if let str = value {
            string = NSMutableAttributedString(string: str, attributes: [NSFontAttributeName: UIFont(name: font,size: CGFloat(size)) as Any])
            string?.addAttribute(NSForegroundColorAttributeName, value: color, range: NSRange(location: 0, length: (str.characters.count)))
            return string!
        } else {
            return string!
        }
    }

}
