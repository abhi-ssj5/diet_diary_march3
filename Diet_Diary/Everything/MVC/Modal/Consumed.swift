//
//  Consumed.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 04/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class Consumed {
    
    var consumed:Food?
    var time:String?
    
    init(name: String, quantity: Int, quantityType: String, value: String, time: String) {
        let totalValue:Double = Double(value)!  * Double(quantity)
        self.consumed = Food(name: name,quantity: quantity, quantityType: quantityType, value: totalValue.description)
        self.time = time
    }
    
    static func all() -> [Consumed] {
        let cornFlakes = Consumed(name: "Cornflakes",
                                  quantity: 2,
                                  quantityType: "Bowl",
                                  value: RandomFunctions.shared.number(value: 200).description,
                                  time: "4:55 pm")
        
        let salad = Consumed(name: "Salad",
                             quantity: 1,
                             quantityType: "Bowl",
                             value: RandomFunctions.shared.number(value: 200).description,
                             time: "6:25 pm")
        
        let pastaZukini = Consumed(name: "Pasta Zukini",
                             quantity: 1,
                             quantityType: "Bowl",
                             value: RandomFunctions.shared.number(value: 200).description,
                             time: "8 pm")
        
        return [cornFlakes, salad, pastaZukini]
    }
    
    
    static func foodConsumedCellData(btn_enable: Int?, object: Consumed?) -> [String: String] {
        let temp = object
        let data:[String: String] = ["name": CheckNil.shared.string(string: temp?.consumed?.foodName),
                                     "time": CheckNil.shared.string(string: temp?.time),
                                     "quantity": CheckNil.shared.number(number: temp?.consumed?.quantity).description,
                                     "quantityType": CheckNil.shared.string(string: temp?.consumed?.quantityType),
                                     "value": CheckNil.shared.string(string: temp?.consumed?.value),
                                     "btn": CheckNil.shared.number(number: btn_enable).description]
        return data
    }
    
}












