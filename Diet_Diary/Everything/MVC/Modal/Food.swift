//
//  Food.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 04/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class Food {
    
    var foodName:String?
    var quantity:Int?
    var quantityType: String?
    var value:String?
    
    init(name: String, quantityType: String, value: String) {
        self.foodName = name
        self.quantity = 1
        self.quantityType = quantityType
        self.value = value
    }
    
    init(name: String, quantity: Int, quantityType: String, value: String) {
        self.foodName = name
        self.quantity = quantity
        self.quantityType = quantityType
        self.value = value
    }
    
    static func all() -> [Food] {
        let cornFlakes = Food(name: "Cornflakes",
                              quantityType: "Bowl",
                              value: RandomFunctions.shared.number(value: 200).description)
        let salad = Food(name: "salad",
                              quantityType: "Bowl",
                              value: RandomFunctions.shared.number(value: 200).description)
        let pastaZukini = Food(name: "pastaZukini",
                              quantityType: "Bowl",
                              value: RandomFunctions.shared.number(value: 200).description)
        let curd = Food(name: "curd",
                              quantityType: "Bowl",
                              value: RandomFunctions.shared.number(value: 200).description)
        let chach = Food(name: "chach",
                              quantityType: "S glass",
                              value: RandomFunctions.shared.number(value: 200).description)
        
        return [cornFlakes, salad, pastaZukini, curd, chach]
    }
    
    static func yourFoodCellData(object: Food?) -> [String: String] {
        let temp = object
        let data:[String: String] = ["name": CheckNil.shared.string(string: temp?.foodName),
                                     "quantity": CheckNil.shared.number(number: temp?.quantity).description,
                                     "quantityType": CheckNil.shared.string(string: temp?.quantityType),
                                     "value": CheckNil.shared.string(string: temp?.value)]
        return data
    }
    
}







