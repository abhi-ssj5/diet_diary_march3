//
//  Day.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation


class Day {
    
    var date:String?
    var categories:[Category]?
    
    init(date: String, categories: [Category]?) {
        self.date = date
        self.categories = categories ?? []
    }
    
}
