//
//  Category.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class Category {
    
    var id:String?
    var name:String?
    var start_time:String?
    var end_time:String?
    var value:String?
    var color:UIColor?
    
    init(id: String, name: String, start_time: String, end_time: String, value: String = "0", color: UIColor = UIColor.white.withAlphaComponent(1)) {
        self.id = id
        self.name = name
        self.start_time = start_time
        self.end_time = end_time 
        self.value = value
        self.color = color
    }
    
    static func all() -> [Category]{
        
        let breakfast = Category(id: "10",
                                 name: "Breakfast",
                                 start_time: "9 am",
                                 end_time: "12 pm",
                                 value: "\(RandomFunctions.shared.number(value: 800))",
            color: RandomFunctions.shared.color())
        
        let lunch = Category(id: "11",
                             name: "Lunch",
                             start_time: "1 pm",
                             end_time: "4 pm",
                             value: "\(RandomFunctions.shared.number(value: 800))",
            color: RandomFunctions.shared.color())
        
        let snacks = Category(id: "12",
                              name: "Snacks",
                              start_time: "",
                              end_time: "",
                              value: "\(RandomFunctions.shared.number(value: 800))",
            color: RandomFunctions.shared.color())
        
        let dinner = Category(id: "13",
                              name: "Dinner",
                              start_time: "8 pm",
                              end_time: "12 pm",
                              value: (RandomFunctions.shared.number(value: 800)).description,
                              color: RandomFunctions.shared.color())
        return [breakfast,lunch,snacks,dinner]
    }
    
}
