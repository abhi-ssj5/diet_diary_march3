//
//  Tab2BreakfastYesterdayViewController.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 03/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class Tab2BreakfastYesterdayViewController: UIViewController {
    
    var itemInfo = IndicatorInfo(title: "Yesterday")
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
}

extension Tab2BreakfastYesterdayViewController: IndicatorInfoProvider {
    
    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
