//
//  Tab1BreakfastTodayViewController.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 03/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class Tab1BreakfastTodayViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddFood: UIButton!
    
    @IBOutlet weak var popUp: UIView!
    
    
    
    let itemInfo = IndicatorInfo(title: "Today")
    var yourFood:[Food]?
    var foodConsumed:[Consumed]?
    let sectionTitle:[String] = ["Items Consumed", "Your Food"]
    var total:Double? = 0
    let recommended:Double = 600
    let firstSectionHeight:CGFloat = 120
    let allSectionHeight:CGFloat = 40
    var selectedRow:Int?
    var showMoreButtons: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        yourFood = Food.all()
        foodConsumed = Consumed.all()
        
        total = totalCaloriesConsumed(array: foodConsumed!)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        
        //add food button
        btnAddFood.layer.borderWidth = 1
        btnAddFood.layer.borderColor = UIColor(red: 0, green: 0.69, blue: 0.94, alpha: 1 ).cgColor

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension Tab1BreakfastTodayViewController {
    
    //compute -> total calories consumed
    func totalCaloriesConsumed(array: [Consumed]) -> Double {
        var sum:Double = 0
        for item in array {
            guard let val = item.consumed?.value else { return 0 }
            sum = sum + Double(val)!
        }
        return sum
    }
    
    //add own food action
    @IBAction func addOwnFoodAction(_ sender: Any) {
        print("add own food")
        showAddFood()
    }
    
    //add own food pop up
    func showAddFood() {
        let modalViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddOwnFood")
        modalViewController?.modalPresentationStyle = .overCurrentContext
        present(modalViewController!, animated: true, completion: nil)
    }
    
    //add quantity pop up
    func showAddQuantity() {
        let modalViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddQuantity")
        modalViewController?.modalPresentationStyle = .overCurrentContext
        present(modalViewController!, animated: true, completion: nil)
    }
    
    //food consumed
    func consumeFood(index: Int) -> Consumed {
        
        let data = Food.yourFoodCellData(object: yourFood?[index])
        let add = Consumed(name: (data["name"])!, quantity: Int(data["quantity"]!)!, quantityType: data["quantityType"]!, value: data["value"]!, time: Time.shared.getTime())
        return add
    }
    
}

//Tab title info
extension Tab1BreakfastTodayViewController: IndicatorInfoProvider {
    
    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

//Table Button Actions
extension Tab1BreakfastTodayViewController {
    
    func tapEdit(sender: UIButton) {
        print(sender.tag, " edit")
        ButtonAnimation.shared.bubble(sender)
    }
    
    func tapDelete(sender: UIButton) {
        print(sender.tag, " delete")
        ButtonAnimation.shared.bubble(sender)
        foodConsumed?.remove(at: sender.tag)
        selectedRow = nil
        let indexpath = IndexPath(row: sender.tag, section: 1)
        tableView.deleteRows(at: [indexpath], with: UITableViewRowAnimation.left)
        self.tableView.delayReload(100)
    }
    
    func tapClear(sender: UIButton) {
        print(sender.tag, " clear")
        selectedRow = nil
        tableView.reloadData()
    }
    
    func tapMore(sender: UIButton) {
        print(sender.tag, " more")
        selectedRow = sender.tag
        tableView.reloadData()
    }
    
    func tapAdd(sender: UIButton) {
        print(sender.tag, " add")
        foodConsumed?.append(consumeFood(index: sender.tag))
        ButtonAnimation.shared.spring(sender)
        
        self.showAddQuantity()
        
        //let indexpath = IndexPath(row: (foodConsumed?.count)! - 1, section: 1)
        //tableView.insertRows(at: [indexpath], with: UITableViewRowAnimation.right)
        selectedRow = nil
        self.tableView.delayReload(100)
    }
    
}


//tableview delegate funstions
extension Tab1BreakfastTodayViewController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return self.firstSectionHeight
        } else {
            return self.allSectionHeight
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 0
            
        case 1:
            return CheckNil.shared.number(number: foodConsumed?.count)
            
        case 2:
            return CheckNil.shared.number(number: yourFood?.count)
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}


//tableview datasource functions
extension Tab1BreakfastTodayViewController: UITableViewDataSource {
    
    //Sections
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch section {
        case 0:
            guard let headerCell = Bundle.main.loadNibNamed("BreakfastTodayFirstSection", owner: self, options: nil)?.first as? BreakfastTodayFirstSection else {
                return UIView.init()
            }
            headerCell.sectionData = [Int(total!), Int(recommended), (Int(recommended - total!))]
            return headerCell
            
        case 1,2:
            guard let headerCell = Bundle.main.loadNibNamed("SectionHeader", owner: self, options: nil)?.first as? SectionHeader else {
                return UIView.init()
            }
            headerCell.lblHead.text = self.sectionTitle[section - 1]
            return headerCell
            
        default:
            return UIView.init()
        }
    }
    
    //Cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            
            guard let cell = Bundle.main.loadNibNamed("FoodConsumedCell", owner: self, options: nil)?.first as? FoodConsumedCell else {
                return UITableViewCell()
            }
            
            cell.btnClear.tag = indexPath.row
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            
            if indexPath.row == selectedRow {
                cell.cellData = Consumed.foodConsumedCellData(btn_enable: 1, object: (foodConsumed?[indexPath.row])!)
                cell.btnDelete.addTarget(self, action: #selector(self.tapDelete), for: .touchUpInside)
                cell.btnClear.addTarget(self, action: #selector(self.tapClear), for: .touchUpInside)
                cell.btnEdit.addTarget(self, action: #selector(self.tapEdit), for: .touchUpInside)
            } else {
                cell.cellData = Consumed.foodConsumedCellData(btn_enable: 0, object: (foodConsumed?[indexPath.row])!)
                cell.btnClear.addTarget(self, action: #selector(self.tapMore), for: .touchUpInside)
            }
            
            return cell
            
        } else if indexPath.section == 2 {
            
            guard let cell = Bundle.main.loadNibNamed("YourFoodCell", owner: self, options: nil)?.first  as? YourFoodCell else {
                return UITableViewCell()
            }
            cell.btnAdd.tag = indexPath.row
            cell.cellData = Food.yourFoodCellData(object: (yourFood?[indexPath.row]))
            cell.btnAdd.addTarget(self, action: #selector(self.tapAdd), for: .touchUpInside)
            return cell
            
        } else {
            return UITableViewCell()
        }
    }

}





























