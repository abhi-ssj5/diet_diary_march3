//
//  Tab3BreakfastSelectDateViewController.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 03/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class Tab3BreakfastSelectDateViewController: UIViewController {
    
    var itemInfo = IndicatorInfo(title: "Select Date")

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension Tab3BreakfastSelectDateViewController: IndicatorInfoProvider {
    
    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
