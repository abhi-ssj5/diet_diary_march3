//
//  BreakFastViewController.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 02/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class BreakFastViewController: ButtonBarPagerTabStripViewController {
    
    let selectedColor = UIColor(red: 0, green: 0.69, blue: 0.94, alpha: 1 )
    let selectedFont = UIFont(name: "Lato-Medium", size: CGFloat(16)) ?? UIFont.systemFont(ofSize: 16)
    let deselectedColor = UIColor(red: 0.76, green: 0.76, blue: 0.76, alpha: 1 )
    let deselectedFont = UIFont(name: "Lato-Regular", size: CGFloat(14)) ?? UIFont.systemFont(ofSize: 14)
    
    override func viewDidLoad() {
        self.onLoadTabProperty()
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = self.deselectedColor
            newCell?.label.textColor = self.selectedColor
            newCell?.label.font = self.selectedFont
        }
        
        super.viewDidLoad()
    }

    func onLoadTabProperty() {
        //button bar properties
        settings.style.selectedBarHeight = 2
        settings.style.buttonBarItemFont = self.deselectedFont
        settings.style.buttonBarItemBackgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1 )
        settings.style.buttonBarItemTitleColor = self.deselectedColor
        settings.style.selectedBarBackgroundColor = self.selectedColor
        settings.style.buttonBarLeftContentInset = 20
        settings.style.buttonBarRightContentInset = 30
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public enum PagerTabStripBehaviour {
        case Common(skipIntermediteViewControllers: Bool)
        case Progressive(skipIntermediteViewControllers: Bool, elasticIndicatorLimit: Bool)
    }
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let today = self.storyboard?.instantiateViewController(withIdentifier: "TodayBreakfast")
        let yeseterday = self.storyboard?.instantiateViewController(withIdentifier: "YesterdayBreakfast")
        let selectDate =  self.storyboard?.instantiateViewController(withIdentifier: "SelectDateBreakfast")
        return [today!, yeseterday!, selectDate!]
    }
}






















