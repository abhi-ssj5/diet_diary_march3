//
//  ViewController.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import PieCharts

class ViewController: UIViewController {
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var pieChart: PieChart!
    @IBOutlet weak var lblTotalValue: UILabel!
    
    var days:[Day]?
    var category:[Category]?
    var daySelected:Int = 0
    
    var dayIndex:Int = 0
    var monthIndex:Int = 0
  
    override func viewDidLoad() {
        super.viewDidLoad()
        pieChart.delegate = self
        days = [createDay()]
        lblTotalValue.attributedText =  generateString(value: "\(Int(computeTotalValue()))")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //create next day
    func createDay() -> Day {
        return Day(date: fetchNextDate(), categories: Category.all())
    }
    
    //fetch next date
    func fetchNextDate() -> String {
        let date = Date()
        let calendar = Calendar.current
        
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        if daySelected == 0 {
            dayIndex = day
            monthIndex = month
            return "\(day)/\(month)/\(year)"
        }
        if dayIndex > 30 {
            dayIndex = 1
            monthIndex += 1
            return "\(dayIndex)/\(month)/\(year)"
        } else {
            dayIndex += 1
            return "\(dayIndex)/\(month)/\(year)"
        }
    }
    
    
    //compute total calories for one day
    func computeTotalValue() -> Double {
        if let categories = days?[daySelected].categories {
            var total:Double = 0
            for slot in categories {
                if (slot.value?.isEmpty ?? true) {
                    total = total + 0
                } else {
                    total = total + Double(slot.value ?? "0")!
                }
            }
            return total
        }
        return 0
    }
    
    //insert new slices in pieChart
    func insertSlices() {
        if let categories = days?[daySelected].categories {
            if categories.isEmpty {
                pieChart.insertSlice(index: 0, model: PieSliceModel(value: 1, color: UIColor.white.withAlphaComponent(1)))
            } else {
                for (index,values) in categories.enumerated() {
                    print(values.value ?? "0")
                    pieChart.insertSlice(index: index, model: PieSliceModel(value: Double(values.value ?? "0")!, color: values.color!))
                }
            }
        }
    }
    
    //generate attributed string for displaying total calories in one day
    func generateString(value: String) -> NSMutableAttributedString {
        let combination = NSMutableAttributedString()
        combination.append(AttributedString.shared.attributeString(value: value, font: "Lato-Light", size: 24))
        combination.append(AttributedString.shared.attributeString(value: " Kcal", font: "Lato-Regular", size: 12))
        return combination
    }

}

extension ViewController {
    
    //navigate between days
    @IBAction func toggleDaysAction(_ sender: UIButton) {
        switch (sender as AnyObject).tag {
        case 1:
            if daySelected > 0 {
                print("prev")
                daySelected -= 1
                self.categoryCollectionView.reloadData()
            }
        case 2:
            print("next")
            daySelected += 1
            days?.insert(createDay(), at: daySelected)
            self.categoryCollectionView.reloadData()
        default:
            break
        }
        lblTotalValue.attributedText =  generateString(value: "\(Int(computeTotalValue()))")
    }
}

//pie chart
extension ViewController: PieChartDelegate {
    func onSelected(slice: PieSlice, selected: Bool) {
        print("Selected: \(selected), slice: \(slice)")
    }
    
    fileprivate func createModels() -> [PieSliceModel] {
        return [PieSliceModel(value: 0, color: UIColor.white.withAlphaComponent(1))]
    }
}

//category collection view
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        lblDay.text = days?[daySelected].date
        pieChart.models = createModels() //create new piechart model
        self.insertSlices() //insert selected day slices
        return (days?[daySelected].categories?.count ?? 0)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! PieChartCollectionViewCell
        cell.cellData = days?[daySelected].categories?[indexPath.row]
        return cell
    }
    
}

//collection view cell layout
extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let heightValue = self.categoryCollectionView.frame.height / 2
        let widthValue = (self.view.frame.width / 2) - 1
        return CGSize(width: widthValue, height: heightValue)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}





