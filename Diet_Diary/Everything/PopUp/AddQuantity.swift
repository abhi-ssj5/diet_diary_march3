//
//  AddQuantity.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 08/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class AddQuantity: UIViewController {
    
    @IBOutlet weak var popUp: UIView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var addFood: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    
    var quantity:Int = 1
    var foodValue:Int = 65

    override func viewDidLoad() {
        super.viewDidLoad()
        updateFields()
        view.clearView()
        popUp.addShadow()
        let ct = Time.shared.getTime()
        lblTime.attributedText = AttributedString.shared.attributeString(value: ct, font: "Lato-Bold", size: 14, color: UIColor(red:0.48, green:0.48, blue:0.48, alpha:1.0))
        addFood.layer.cornerRadius = 2
    }

    //toggle quantity
    @IBAction func changeQuantity(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            if quantity > 0 {
                quantity -= 1
            }
        case 1:
            quantity += 1
        default: break
        }
        updateFields()
    }
    
    //update quantity and value
    func updateFields() {
        updateLbl(str1: quantity.description, str2: " Bowl", label: lblQuantity)
        let temp = foodValue * quantity
        updateLbl(str1: temp.description, str2: " Kcal", label: value)
        
    }
    
    //generate attributed combination string
    func updateLbl(str1: String, str2: String, label: UILabel) {
        let combination = NSMutableAttributedString()
        combination.append(AttributedString.shared.attributeString(value: str1, font: "Lato-Medium", size: 14, color: UIColor(red:0.55, green:0.55, blue:0.55, alpha:1.0)))
        combination.append(AttributedString.shared.attributeString(value: "\n"+str2, font: "Lato-Regular", size: 12, color: UIColor(red:0.76, green:0.76, blue:0.76, alpha:1.0)))
        label.attributedText = combination
    }
    
    @IBAction func addFoodAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
}
