//
//  AddOwnFood.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 08/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class AddOwnFood: UIViewController {
    
    @IBOutlet weak var popUp: UIView!
    
    @IBOutlet weak var txtFoodName: UITextField!
    @IBOutlet weak var txtMeasurementUnit: UITextField!
    @IBOutlet weak var txtValue: UITextField!
    
    @IBOutlet weak var btnAddFood: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.clearView()
        popUp.addShadow()
        btnAddFood.layer.cornerRadius = 2
    }
    
    func dismissPopUp() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismissPopUp()
    }
    
    @IBAction func actionAddFood(_ sender: Any) {
        dismissPopUp()
    }
    
    
}
