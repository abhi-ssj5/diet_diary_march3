//
//  BreakfastTodayFirstSection.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 04/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class BreakfastTodayFirstSection: UITableViewCell {
    
    
    @IBOutlet weak var lblCaloriesConsumed: UILabel!
    @IBOutlet weak var lblRecommend: UILabel!
    @IBOutlet weak var lblRemaining: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var max:Int = 100
    var current:Int = 0
    
    var sectionData:[Int]? {
        didSet {
            updateCell()
        }
    }
    
    func updateCell() {
        self.lblCaloriesConsumed.text = sectionData?[0].description
        self.lblRecommend.text = sectionData?[1].description
        self.lblRemaining.text = sectionData?[2].description
        
        max = sectionData![1]
        current = sectionData![0]
        upgradeProgress()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func upgradeProgress() {
        let i = current
        if i <= max {
            let ratio = Float(i) / Float(max)
            progressBar.progress = Float(ratio)
            current += 1
        }
        
    }
    
}










