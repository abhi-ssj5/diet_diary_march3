//
//  SectionHeader.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 04/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class SectionHeader: UITableViewCell {

    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var sectionId:Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
