//
//  FoodConsumedCell.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 06/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class FoodConsumedCell: UITableViewCell {

    @IBOutlet weak var lblFood: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    
    var cellData:[String:String]? {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        guard let data = cellData else {
            print("Empty Cell")
            return
        }
        
        self.lblFood.numberOfLines = 0
        self.lblFood.lineBreakMode = .byWordWrapping
        self.lblFood.attributedText = TodayCellLabel.shared.attStringLabel(data, section: .foodConsumed)
        
        if cellData?["btn"] == "1" {
            cellSelected()
        } else {
            cellDeselected()
        }
    }
    
    func cellSelected() {
        btnEdit.isHidden = false
        btnEdit.isEnabled = true
        btnDelete.isHidden = false
        btnDelete.isEnabled = true
        ButtonAnimation.shared.spring(btnEdit)
        ButtonAnimation.shared.spring(btnDelete)
        ButtonAnimation.shared.spring(btnClear)
    }
    
    func cellDeselected() {
        btnEdit.isHidden = true
        btnEdit.isEnabled = false
        btnClear.setImage(UIImage(named: "ic_more_vert"), for: .normal)
        btnDelete.isHidden = true
        btnDelete.isEnabled = false
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
