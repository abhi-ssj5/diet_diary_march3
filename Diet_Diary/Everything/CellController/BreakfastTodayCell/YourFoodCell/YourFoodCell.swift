//
//  YourFoodCell.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 06/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class YourFoodCell: UITableViewCell {
    
    @IBOutlet weak var lblFood: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    var cellData:[String: String]? {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        guard let data = cellData else {
            print("Empty Cell")
            return
        }
        
        self.lblFood.numberOfLines = 0
        self.lblFood.lineBreakMode = .byWordWrapping
        lblFood.attributedText = TodayCellLabel.shared.attStringLabel(data, section: .yourFood)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
