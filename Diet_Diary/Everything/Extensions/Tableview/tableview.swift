//
//  tableview.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 08/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func delayReload(_ milliSeconds: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(milliSeconds), execute: {
            self.reloadData()
        })
    }
}
